/*
 * HueLamp.h
 *
 *  Created on: 28.05.2021
 *      Author: student
 */

#ifndef HUELAMP_H_
#define HUELAMP_H_

#include "yahal_String.h"
#include "HueValue.h"

/**
 * This Modelclass combines the Datafields of a HueLamp.
 */
class HueLamp {
private:
    HueValue value;

    int id = -1;
    String name = "";
    bool reachable = 1;

public:
    HueLamp() {}
    HueLamp(int id) : id(id){}
    ~HueLamp(){}

    int getId() const
    {
        return id;
    }

    void setId(int id)
    {
        this->id = id;
    }

    const String& getName() const
    {
        return name;
    }

    void setName(String name)
    {
        this->name = name;
    }

    const HueValue getValue() const
    {
        return value;
    }

    void setValue(HueValue value)
    {
        this->value = value;
    }

    bool isReachable() {
        return this->reachable;
    }

    void setReachable(bool value) {
        this->reachable = value;
    }

    String toString() {
        return String("HueLamp{id:"+to_String(id))+
                String(", name:"+name)+
                String(", reachable:"+to_String(reachable))+
                String(",value:"+value.toString())+
                String("}");
    }
};



#endif /* HUELAMP_H_ */
