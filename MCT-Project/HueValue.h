/*
 * HueValue.h
 *
 *  Created on: 28.05.2021
 *      Author: student
 */

#ifndef HUEVALUE_H_
#define HUEVALUE_H_

#include <cstdint>
#include "posix_io.h"

#define MAX_HUE 65535
#define MAX_SATURATION 255
#define MAX_BRIGHTNESS 255

/**
 * This Modelclass contains the datafields for the value a lamp can accept.
 */
class HueValue {
    private:
    uint8_t saturation;
    uint8_t brightness;
    uint16_t hue;
    bool onStatus;

    public:
    HueValue(): HueValue(0, 0, 0, false) {

    }
    HueValue(uint8_t saturation, uint8_t brightness, uint16_t hue, bool onStatus):
        saturation(saturation), brightness(brightness), hue(hue), onStatus(onStatus) {
    }

    /**
     * @deprecated use individual percentXX() instead.
     */
    void percentValue(int huePercent, int satPercent) {
        float percentX = huePercent/(float)100.;
        float percentY = satPercent/(float)100.;

        uint16_t hue = percentX * MAX_HUE;
        uint8_t saturation = percentY * MAX_SATURATION;

        this->hue = hue;
        this->saturation = saturation;
    }

    void percentSaturation(int satPercent) {
        float percent = satPercent/100.;
        this->saturation = percent * MAX_SATURATION;
    }

    void percentBrightness(int briPercent) {
        float percent = briPercent / 100.;

        this->brightness = percent * MAX_BRIGHTNESS;
    }

    void percentHue(int vPercent) {
        float percent = vPercent / 100.;
        this->hue = percent * MAX_HUE;
    }

    void toXY(int &x, int &y) {
        float percentX = this->hue /  (float) MAX_HUE;
        float percentY = this->saturation / (float) MAX_SATURATION;

        x = percentX * 100;
        y = percentY * 100;
    }

    void setSaturation(uint8_t saturation) {
        this->saturation = saturation;
    }


    void setBrightness(uint8_t brightness) {
        this->brightness = brightness;
    }

    void setHue(uint16_t hue) {
        this->hue = hue;
    }

    void setOnStatus(bool onStatus) {
        this->onStatus = onStatus;
    }

    uint8_t getBrightness() const
    {
        return brightness;
    }

    uint16_t getHue() const
    {
        return hue;
    }

    bool getOnStatus() const
    {
        return onStatus;
    }

    uint8_t getSaturation() const
    {
        return saturation;
    }

    String toString() {
        return String("HueValue{") +
                "sat:" + to_String(saturation) +
                ", bri: " + to_String(brightness) +
                ", hue: " + to_String(hue) +
                ", status: " + to_String(onStatus) +
                "}";
    }
};



#endif /* HUEVALUE_H_ */
