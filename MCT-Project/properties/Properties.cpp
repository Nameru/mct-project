
#include "Properties.h"
#include "InMemoryProperties.h"

// this is needed to prevent compile errors
Properties* Properties::instance = 0;


Properties* Properties::getInstance() {
    if(instance == 0) {
        instance = new InMemoryProperties();
    }
    return instance;
}

void Properties::setInstance(Properties* inst) {
    instance = inst;
}
