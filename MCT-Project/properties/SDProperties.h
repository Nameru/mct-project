/*
 * SDProperties.h
 *
 *  Created on: 05.06.2021
 *      Author: student
 */

#ifndef PROPERTIES_SDPROPERTIES_H_
#define PROPERTIES_SDPROPERTIES_H_

#include <properties/Properties.h>
#include "gpio_interface.h"
#include "spi_msp432.h"

/**
 * The SDProperties class reads properties from the SD card.
 *
 * Properties are saved as a simple text file with a custom name.
 */
class SDProperties: public Properties {
private:
    String ssid;
    String key;
    String userId;
    String bridgeIp;

    String filename;
    gpio_pin_t sdPin;
    EUSCI_A_SPI_Type *spi_type;

    bool sdStatus = false;
    void parseLine(String);

public:

    SDProperties(String filename, gpio_pin_t sdPin = PORT_PIN(2, 0), EUSCI_A_SPI_Type *spi_type = EUSCI_A1_SPI)
        : filename(filename), sdPin(sdPin), spi_type(spi_type) {
    }

    ~SDProperties();

    String getSSID() override{
        return ssid;
    }

    void setSSID(String ssid) override{
        this->ssid = ssid;
    }
    String getKey() override{
        return key;
    }

    void setKey(String key) override{
        this->key = key;
    }

    String getApiUserId() override{
        return userId;
    }

    void setApiUserId(String userId) override{
        this->userId = userId;
    }

    void setBridgeIp(String ip) override{
        this->bridgeIp = ip;
    }

    String getBridgeIp() override {
        return bridgeIp;
    }


    bool getSdStatus() {
        return sdStatus;
    }

    void save() override;

    bool load() override;
};



#endif /* PROPERTIES_SDPROPERTIES_H_ */
