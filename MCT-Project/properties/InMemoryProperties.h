/*
 * InMemoryPropierties.h
 *
 *  Created on: 13.07.2021
 *      Author: student
 */

#ifndef PROPERTIES_INMEMORYPROPERTIES_H_
#define PROPERTIES_INMEMORYPROPERTIES_H_

#include "Properties.h"
#include "../Config.h"


/**
 * This class can be used if one wants to use hardcoded connection
 * Data instead of reading the SD card
 */
class InMemoryProperties : public Properties {
public:
    String ssid = WIFI_SSID;
    String key  = WIFI_KEY;
    String apiUserId = HUE_USER_ID;
    String bridgeIp = ""; // unusued

    String getSSID() override {
        return ssid;
    }

    String getKey() override {
        return key;
    }

    String getApiUserId() override {
        return apiUserId;
    }

    String getBridgeIp() override {
        return bridgeIp;
    }

    void setSSID(String ssid) override {
        this->ssid = ssid;
    }

    void setKey(String key) override {
        this->key = key;
    }

    void setApiUserId(String userId) {
        this->apiUserId = userId;
    }

    void setBridgeIp(String ip) override{
        this->bridgeIp = ip;
    }

    void save() override {
        // not implemented because of hardcoded values
    }

    bool load() override {
        // not implemented because of hardcoded values
        return true;
    }
};

#endif /* PROPERTIES_INMEMORYPROPERTIES_H_ */
