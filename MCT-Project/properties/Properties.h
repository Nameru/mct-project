/*
 * Properties.h
 *
 *  Created on: 05.06.2021
 *      Author: student
 */

#ifndef PROPERTIES_PROPERTIES_H_
#define PROPERTIES_PROPERTIES_H_

#include "yahal_String.h"


/**
 * This Properties class provides a basic interface for retrieving information i. e. the wifi ssid.
 */
class Properties {
private:
    static Properties* instance;

public:

    static Properties* getInstance();
    static void setInstance(Properties*);

    virtual String getSSID() = 0;
    virtual String getKey() = 0;
    virtual String getApiUserId() = 0;
    virtual String getBridgeIp() = 0;

    virtual void setSSID(String) = 0;
    virtual void setKey(String) = 0;
    virtual void setApiUserId(String) = 0;
    virtual void setBridgeIp(String) = 0;

    virtual void save() = 0;
    virtual bool load() = 0;
};

#endif /* PROPERTIES_PROPERTIES_H_ */
