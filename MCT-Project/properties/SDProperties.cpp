/*
 * SDProperties.cpp
 *
 *  Created on: 05.06.2021
 *      Author: student
 */

#include <properties/SDProperties.h>
#include "yahal_assert.h"
#include "ff.h"
#include "gpio_msp432.h"
#include "sd_spi_drv.h"
#include "spi_msp432.h"
#include "posix_io.h"

#include <cstdio>
#include <cassert>
#include <cstring>



SDProperties::~SDProperties() {

}

void SDProperties::parseLine(String line) {
    int split = line.find('=');
    String prop = line.substr(0, split);
    String value = line.substr(split+1, line.size()-split-2);

    if(prop.operator ==("ssid")) {
        ssid = value;
    } else if(prop.operator ==("key")) {
        key = value;
    } else if(prop.operator ==("userId")) {
        userId = value;
    } else {
        printf("Unknown property '%s'\n", prop.c_str());
    }
}

bool SDProperties::load() {
    printf("Loading properties from SD-Card\n");
    // Set up the driver stack for the SD card
    //////////////////////////////////////////
    gpio_msp432_pin cs ( this->sdPin );     // CS Line of SPI interface
    spi_msp432      spi( this->spi_type, cs ); // SPI interface connected to SD card
    sd_spi_drv      sd ( spi );        // SD card low level driver
    FatFs           fs ( sd );         // FatFs driver

    posix_io::inst.register_fileio(fs);
    if(fs.mount() != FatFs::FR_OK) {
        sdStatus = false;
        return false;
    }

    FILE *f = fopen(filename.c_str(), "r");
    if(f == NULL) {
        printf("cannot open file\n");
        sdStatus = false;
        return false;
    }
    printf("File opened\n");

    char buf[200], *s;
    fseek(f, 0, SEEK_SET);

    do {
        s = fgets(buf, 200, f);
        parseLine(buf);
     } while (s);

    fclose(f);
    if(fs.umount() != FatFs::FR_OK) {
        sdStatus = false;
        return false;
    }

    printf("Properties loaded successfully\n");
    sdStatus = true;
    return true;
}


void SDProperties::save() {
    // Set up the driver stack for the SD card
    //////////////////////////////////////////
    gpio_msp432_pin cs ( this->sdPin );     // CS Line of SPI interface
    spi_msp432      spi( this->spi_type, cs ); // SPI interface connected to SD card
    sd_spi_drv      sd ( spi );        // SD card low level driver
    FatFs           fs ( sd );         // FatFs driver

    posix_io::inst.register_fileio(fs);
    yahal_assert(fs.mount() == FatFs::FR_OK);

    FILE *f = fopen(filename.c_str(), "w+");
    if(f == NULL) {
        printf("cannot open file\n");
        yahal_assert(f != NULL);
    }
    printf("File opened\n");

    fprintf(f, "ssid=%s\n", this->ssid.c_str());
    fprintf(f, "key=%s\n", this->key.c_str());
    fprintf(f, "userId=%s\n", this->userId.c_str());

    fclose(f);
    yahal_assert(fs.umount() == FatFs::FR_OK);

    printf("Properties saved successfully\n");
}



