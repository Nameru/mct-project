// ---------------------------------------------
//           This file is part of
//      __    __     ____    _     __    __
//     /  \  /  \   / __ \  | |   /  \  / /
//    / /\ \/ /\ \  | \/ |  | |  / /\ \/ /
//   /_/  \__/  \_\ |_/\_|  |_| /_/  \__/
//
// ---------------------------------------------


#include <cstring>
#include <cstdio>

#include <functional>
using std::function;

#include "posix_io.h"
#include "uart_msp432.h"

#include "gpio_msp432.h"
#include "spi_msp432.h"


#include "msp.h"

#include "st7735s_drv.h"
#include "task.h"
#include "task_monitor.h"

#include "Config.h"
#include "controls/LoopButtonController.h"
#include "model/RestFacade.h"
#include "properties/SDProperties.h"
#include "properties/InMemoryProperties.h"
#include "LampManager.h"
#include "gui/LampGui.h"
#include "gui/connection/ConnectionView.h"

void connectAndStart();
void initProperties();

bool sdLoadStatus = false;


int main(void)
{
    // Setup backchannel UART
    uart_msp432 uart;
    posix_io::inst.register_stdout( uart );
    posix_io::inst.register_stderr( uart );
    printf("Start MSP\n");

    initProperties();
    connectAndStart();

}


void connectAndStart() {
        printf("Starting main task\n");
        // Forward any chars from the ESP8266 UART to the back channel UART
        // so we can see the debug messages from the ESP8266!
        uart_msp432 uart_esp(EUSCI_A3,115200);
        String answer;
        uart_esp.uartAttachIrq([&](char c) {
            if (c == '\n') {
                printf("before init from ESP8266: %s\n", answer.c_str());
                answer.clear();
            } else {
                answer += c; // Add char to message
            }
        });

        printf("Init ESP\n");

        // ESP Setup
        gpio_msp432_pin esp_reset( PORT_PIN(10, 5) );
        esp_reset.gpioMode(GPIO::OUTPUT | GPIO::INIT_LOW);
        task::sleep(200);
        esp_reset.gpioWrite( HIGH );
        task::sleep(200);


        gpio_msp432_pin lcd_bl (PORT_PIN(2, 6));
        lcd_bl.gpioMode(GPIO::OUTPUT | GPIO::INIT_HIGH);

        printf("Init LCD\n");

        // Setup SPI interface
        gpio_msp432_pin lcd_cs (PORT_PIN(5, 0));
        spi_msp432  spi(EUSCI_B0_SPI, lcd_cs);
        spi.setSpeed(24000000);

        // Setup LCD driver
        gpio_msp432_pin lcd_rst(PORT_PIN(5, 7));
        gpio_msp432_pin lcd_dc (PORT_PIN(3, 7));
        st7735s_drv lcd(spi, lcd_rst, lcd_dc, st7735s_drv::Crystalfontz_128x128);


        ///////////// CREATING OBJECTS //////////////////

        RestFacade facade(&uart_esp);

        /**
         * Main task - waiting and responding to button interactions
         */
        task* mainTask = new task([&]() {
            printf("Starting main Task\n");
            LampModel model(&facade);
            LampManager man(&model);
            LoopButtonController btn(&man);

            LampGui gui(&man, &lcd);
            man.setView(&gui);
            man.reloadLamps();
            btn.startAllTasks();

            while(true){
                task::yield();
            }
        }, "mainTask", 16384); // 2^14

        /**
         * Connect to bridge.
         */
        task* connect = new task([&](){
            printf("Starting connection task\n");
            facade.establishConnection();
            printf("Connection established\n");
        }, "connect", 2048);

        /**
         * Show GUI with connection status
         */
        task* connectionGui = new task([&](){
            ConnectionView view(&lcd);
            view.setSDConnected([](){return sdLoadStatus;});
            view.setNetworkConnectied([&]() {return facade.getNetworkConnection();});
            view.setBridgeConnected([&]() {return facade.getBridgeConnection();});
            view.setNeedPushButton([&](){ return facade.getPushButton();} );
            view.setWhereBridge([&]()   { return facade.getWhereBridge();} );

            printf("Starting gui task\n");
            while(!facade.getBridgeConnection()) {
                view.tick();
                task::sleep(1000);
            }
            view.tick();
            task::sleep(2000);

            printf("Facade is Connected. Starting real program\n");
            // Setting up program

            mainTask->start();
        }, "connectionGui", 8192);



        connect->start();
        connectionGui->start();

        task_monitor monitor;
        //monitor.start();


        printf("Finished all init. Starting task scheduler.\n");
        task::start_scheduler();
}


void printProperties() {
    Properties* prop = Properties::getInstance();

    printf("--- Using Properties: ---\n");
    printf("Wifi-SSID: %s\n", prop->getSSID().c_str());
    printf("Wifi-Key: %s\n", prop->getKey().c_str());
    printf("UserId: %s\n", prop->getApiUserId().c_str());
    printf("-------------------------\n");
}
void useConstantProperties() {
    Properties* prop = Properties::getInstance();

    prop->setSSID(WIFI_SSID);
    prop->setKey(WIFI_KEY);
    prop->setApiUserId(HUE_USER_ID);

    prop->save();
}


void initProperties() {
    if(USE_SD) {
        SDProperties* prop = new SDProperties(SD_FILENAME);
        Properties::setInstance(prop);

        sdLoadStatus = prop->load();
        if(!prop->getSdStatus() || !sdLoadStatus) {
            printf("Using fallback properties\n");
            delete prop;
            Properties::setInstance(new InMemoryProperties());
            useConstantProperties();
        }
    }

    if(FORCE_WIFI_INFO || !USE_SD) {
        useConstantProperties();
    }
    printProperties();
}
