/*
 * LampManager.h
 *
 *  Created on: 28.05.2021
 *      Author: student
 */

#ifndef LAMPMANAGER_H_
#define LAMPMANAGER_H_

#include "model/LampModel.h"
#include "HueLamp.h"
#include "gui/Observer.h"

#define MAX_LAMPS 8

/**
 * This class manages all lamps.
 * It is in control of the currently selected lamp and the (temporary) new value that can be applied to the lamp.
 *
 * The communication to the bridge happens through the abstraction layer of the LampModel.
 *
 * An Observer can be attached to get notified on changes. This is used for the gui to update Information.
 *
 */
class LampManager{
private:
    Observer *view = 0;
    LampModel *model;
    HueLamp *lamps;

    int selectedLamp;
    int lampCount = 0;

    // these values represent the new colors with integers between 0 and 100 (percentages)
    // they can be used by the view to track specific values of the new value
    int colorHue = 0;
    int colorBri = 100;
    int colorSat = 100;

    HueValue newValue;

    ControlState controlState;

public:
    LampManager(LampModel*);
    ~LampManager();

    void setView(Observer *observer);
    void notifyView(ChangeEvent=ALL);


    int getLampCount();
    void addLamp(HueLamp);
    void reloadLamps();
    void selectLamp(int index);
    bool containsLampWithHueId(int id);
    HueLamp getLampByHueId(int id);
    HueLamp getSelectedLamp();
    int getSelectedLampId();

    void printLamps();

    ControlState getControlState();
    void changeValueHue(int dx, int dy);
    void changeValueSaturation(int dy);
    HueValue getNewValue();

    int getColorX();
    int getColorY();
    int getColorSat();

    /***** button interactions *****/
    void establishNetworkConnection();

    void applyValue();
    void selectNextLamp();

    /*
     * Toggles what the stick is in control of.
     * The three states are:
     * 1) hue/saturation control with x/y axis
     * 2) Brightness control with y axis
     * 3) Turn the lamp off/on (opposite of current status)
     */
    void toggleControlState();

    /**
     * This method registers a change in
     * value and delegates them to the correct sub-method.
     */
    void moveValueCursor(int dx, int dy);

};


#endif /* LAMPMANAGER_H_ */
