/*
 * Observer.h
 *
 *  Created on: 19.07.2021
 *      Author: student
 */

#ifndef GUI_OBSERVER_H_
#define GUI_OBSERVER_H_


/**
 * This events reduce time when re-drawing the gui.
 * They split different actions.
 */
enum ChangeEvent {
    ALL, LAMP, COLOR, STATE, POINTER
};

/**
 * The Observer is notified when anything in the LampManager updates.
 * This is needed to remove a circular reference from View <-> Viewmodel
 */
class Observer {
public:
    virtual void notify(ChangeEvent) = 0;
};
enum ControlState {
        HUE, BRIGHTNESS, OFF
};


#endif /* GUI_OBSERVER_H_ */
