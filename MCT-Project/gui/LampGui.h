/*
 * LampGui.h
 *
 *  Created on: 31.05.2021
 *      Author: student
 */

#ifndef LAMPGUI_H_
#define LAMPGUI_H_

#include "LampManager.h"
#include "gpio_msp432.h"
#include "spi_msp432.h"
#include "st7735s_drv.h"

#include "lcd_interface.h"
#include "uGUI.h"
#include "GuiConstants.h"
#include "Observer.h"

/**
 * The LampGui is the main GUI of this application.
 * It observes the LampManager and reacts when notified.
 *
 * Notifications are split in different categories to reduce redrawing time.
 */
class LampGui: public Observer {

private:
    LampManager *manager;
    lcd_interface *lcd;
    uGUI gui;


public:

    LampGui(LampManager *manager, lcd_interface *lcd);

    void drawBackground();
    void drawStartUp();
    void drawCurrentColor();
    void drawLampSelection();
    void drawColorSelection();
    void drawNewColor();
    void drawSatBar();
    void drawStateSelection();

    void drawColorSelectionPointer();
    void drawSatBarPointer();

    // this is used for debug purpose
    void drawBoundingBoxes();


    void notify(ChangeEvent) override;
};



#endif /* LAMPGUI_H_ */
