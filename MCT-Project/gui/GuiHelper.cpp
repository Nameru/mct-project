

#include "GuiHelper.h"

using namespace guiHelper;

/**
 * convert rgb to int.
 * 8 bit Red, 8bit green, 8bit blue
 */
UG_COLOR guiHelper::rgbToInt(rgb in) {
    const int max = 255;
    return ((int)(in.r * max) << 16) | ((int)(in.g * max) << 8) | (int)(in.b * max);
}

/**
 * Draw the left lamp selection arrow inside the given bounding box
 */
void guiHelper::drawLampSelectionArrowLeft(uGUI &gui, int x, int y, int width, int height, UG_COLOR c) {

    // draw left and right arrow
    gui.DrawLine(x, y + height/2, x + width, y, c);
    gui.DrawLine(x, y + height/2, x + width, y+height, c);
}

/**
 * Draw the right lamp selection arrow inside the given bounding box
 */
void guiHelper::drawLampSelectionArrowRight(uGUI &gui, int x, int y, int width, int height, UG_COLOR c) {

    // draw left and right arrow
    gui.DrawLine(x, y , x + width, y+ height/2, c);
    gui.DrawLine(x, y +height, x + width, y+ height/2, c);
}

void guiHelper::drawDynamicCircles(uGUI &gui, int x, int y, int width, int height, int minSpacing, int amount, int selectedIndex, UG_COLOR c) {
    int circleY = y + height /2;

    float spacing = width/(float)(amount+1);
    int radius = LAMP_SELECTION_MAX_DIAMETER /2;

    if(spacing < 2*radius + minSpacing) {
        radius = (spacing - minSpacing)/2;
    }

    for(int i = 1; i <= amount; ++i) {

        // selectedIndex is based on 0-counting
        if(i == selectedIndex+1) {
            gui.FillCircle(x + i*spacing, circleY, radius, c);
        } else {
            gui.DrawCircle(x + i*spacing, circleY, radius, c);
        }
    }
}

void guiHelper::drawText(uGUI &gui, String text, int x, int y, const uGUI::FONT *font, UG_COLOR c, UG_COLOR back) {
    gui.SetForecolor(c);
    gui.SetBackcolor(back);
    gui.FontSelect(font);
    gui.PutString(x, y, text.c_str());
}

void guiHelper::drawHueColorMatrixPart(uGUI &gui, int x, int y, float pX, float pY) {
    // max values for hsv struct. used for scaling
    const uint16_t maxHue = 360;
    const uint8_t maxVal = 1;
    hsv hsv;
    hsv.s = 1;
    hsv.h = pX * maxHue;
    hsv.v = maxVal - pY * maxVal; // decrease value with increasing Ys
    rgb rgb = hsv2rgb(hsv);
    gui.FillFrame(x, y, x, y, rgbToInt(rgb));
}

void guiHelper::drawHueColorMatrix(uGUI &gui, int x, int y, int width, int height) {
    float percentX, percentY;
    for( int i = 0; i <= width; ++i) {
        for ( int k = 0; k <= height; ++k) {
            percentX = i/(float)width;
            percentY = k/(float)height;
            drawHueColorMatrixPart(gui, x+i, y+k, percentX, percentY);
        }
    }
}

void guiHelper::drawSatColorPart(uGUI &gui, hsv hsv, int x, int y, int width, float pY) {
    const uint8_t maxSat = 1; // max value for hsv struct
    hsv.s = maxSat - pY * maxSat; // decrease with increasing Ys
    rgb rgb = hsv2rgb(hsv);
    gui.FillFrame(x, y, x+width, y, rgbToInt(rgb));
}

void guiHelper::drawSatColorBar(uGUI &gui, HueValue& v, int x, int y, int width, int height) {
    hsv hsv = valueToHsv(v);

    float percentY = 0;
    for(int k = 0; k <= height; ++k) {
        percentY = k/(float) height;
        drawSatColorPart(gui, hsv, x, y+k, width, percentY);
    }
}

void guiHelper::drawSatPointer(uGUI &gui, HueValue& v, int colorSat, int x, int y, int width, int height, int dy) {
    hsv hsv = valueToHsv(v);

    float percentY;
    int start = colorSat / 100. * height - dy;
    int end = colorSat / 100. * height + dy;

    start = start < y ? y: start;
    end = end > y + height ? y + height: end;

    for(int i = start; i <= end; i++) {
        percentY = i / (float) height;
        drawSatColorPart(gui, hsv, x, y+i, width, percentY);
    }
    //gui.FillFrame(x, colorSat/100. * height, x + width, colorSat/100. * height, 0x000000);
}

void guiHelper::drawStateArea(uGUI &gui, ControlState selectedState) {
    UG_COLOR stateColor = STATE_UNSELECTED_COLOR;
    UG_COLOR stateTextColor = STATE_SELECTED_COLOR;
    UG_COLOR hueColor = STATE_UNSELECTED_COLOR;
    UG_COLOR hueTextColor = STATE_SELECTED_COLOR;
    UG_COLOR briColor = STATE_UNSELECTED_COLOR;
    UG_COLOR briTextColor = STATE_SELECTED_COLOR;

    switch(selectedState) {
    case OFF:
        stateColor = STATE_SELECTED_COLOR;
        stateTextColor = STATE_UNSELECTED_COLOR;
        break;
    case HUE:
        hueColor = STATE_SELECTED_COLOR;
        hueTextColor = STATE_UNSELECTED_COLOR;
        break;
    case BRIGHTNESS:
        briColor = STATE_SELECTED_COLOR;
        briTextColor = STATE_UNSELECTED_COLOR;
    }

    gui.FillFrame(STATUS_STATE_X,
                  STATUS_STATE_Y,
                  STATUS_STATE_X + STATUS_STATE_WIDTH,
                  STATUS_STATE_Y + STATUS_STATE_HEIGHT,
                  stateColor);

    gui.FillFrame(STATUS_HUE_X,
                  STATUS_HUE_Y,
                  STATUS_HUE_X + STATUS_HUE_WIDTH,
                  STATUS_HUE_Y + STATUS_HUE_HEIGHT,
                  hueColor);

    gui.FillFrame(STATUS_BRI_X,
                  STATUS_BRI_Y,
                  STATUS_BRI_X + STATUS_BRI_WIDTH,
                  STATUS_BRI_Y + STATUS_BRI_HEIGHT,
                  briColor);

    // use opposite color for text
    int offsetX = 5;
    int offsetY = 1;

    drawText(gui, STATUS_STATE_STRING,
             STATUS_STATE_X + offsetX,
             STATUS_STATE_Y + offsetY,
             &FONT_5X8,
             stateTextColor,stateColor);

    drawText(gui, STATUS_HUE_STRING,
             STATUS_HUE_X + offsetX,
             STATUS_HUE_Y + offsetY,
             &FONT_5X8
             , hueTextColor,hueColor);

    drawText(gui, STATUS_BRI_STRING,
             STATUS_BRI_X + offsetX,
             STATUS_BRI_Y + offsetY,
             &FONT_5X8, briTextColor,
             briColor);

}

void guiHelper::drawBoundingBoxesBasedOnConstants(uGUI &gui) {
    gui.FillFrame(LAMP_SELECTION_X,
                  LAMP_SELECTION_Y,
                  LAMP_SELECTION_X + LAMP_SELECTION_WIDTH,
                  LAMP_SELECTION_Y + LAMP_SELECTION_HEIGHT,
                  C_DARK_OLIVE_GREEN);

    gui.FillFrame(CURRENT_VALUE_X,
                  CURRENT_VALUE_Y,
                  CURRENT_VALUE_X + CURRENT_VALUE_WIDTH,
                  CURRENT_VALUE_Y + CURRENT_VALUE_HEIGHT,
                  C_RED);

    gui.FillFrame(NEXT_VALUE_X,
                  NEXT_VALUE_Y,
                  NEXT_VALUE_X + NEXT_VALUE_WIDTH,
                  NEXT_VALUE_Y + NEXT_VALUE_HEIGHT,
                  C_BLUE);

    gui.FillFrame(COLOR_MATRIX_X,
                  COLOR_MATRIX_Y,
                  COLOR_MATRIX_X + COLOR_MATRIX_WIDTH,
                  COLOR_MATRIX_Y + COLOR_MATRIX_HEIGHT,
                  C_GREEN);

    gui.FillFrame(STATUS_STATE_X,
                  STATUS_STATE_Y,
                  STATUS_STATE_X + STATUS_STATE_WIDTH,
                  STATUS_STATE_Y + STATUS_STATE_HEIGHT,
                  C_PURPLE);

    gui.FillFrame(STATUS_HUE_X,
                  STATUS_HUE_Y,
                  STATUS_HUE_X + STATUS_HUE_WIDTH,
                  STATUS_HUE_Y + STATUS_HUE_HEIGHT,
                  C_YELLOW);

    gui.FillFrame(STATUS_BRI_X,
                  STATUS_BRI_Y,
                  STATUS_BRI_X + STATUS_BRI_WIDTH,
                  STATUS_BRI_Y + STATUS_BRI_HEIGHT,
                  C_AQUA);
}

void guiHelper::drawFramesBasedOnConstants(uGUI &gui, UG_COLOR color) {
    gui.DrawFrame(CURRENT_VALUE_X-1,
                  CURRENT_VALUE_Y-1,
                  CURRENT_VALUE_X + CURRENT_VALUE_WIDTH + 1,
                  CURRENT_VALUE_Y + CURRENT_VALUE_HEIGHT + 1, color);
    gui.DrawFrame(NEXT_VALUE_X-1,
                  NEXT_VALUE_Y-1,
                  NEXT_VALUE_X + NEXT_VALUE_WIDTH + 1,
                  NEXT_VALUE_Y + NEXT_VALUE_HEIGHT + 1, color);
    gui.DrawFrame(COLOR_MATRIX_X-1,
                  COLOR_MATRIX_Y-1,
                  COLOR_MATRIX_X + COLOR_MATRIX_WIDTH + 1,
                  COLOR_MATRIX_Y + COLOR_MATRIX_HEIGHT + 1, color);
    gui.DrawFrame(COLOR_SAT_X-1,
                  COLOR_SAT_Y-1,
                  COLOR_SAT_X + COLOR_SAT_WIDTH + 1,
                  COLOR_SAT_Y + COLOR_SAT_HEIGHT + 1, color);
    gui.DrawFrame(STATUS_STATE_X-1,
                  STATUS_STATE_Y-1,
                  STATUS_STATE_X + STATUS_STATE_WIDTH + 1,
                  STATUS_STATE_Y + STATUS_STATE_HEIGHT + 1, color);
    gui.DrawFrame(STATUS_HUE_X-1,
                  STATUS_HUE_Y-1,
                  STATUS_HUE_X + STATUS_HUE_WIDTH + 1,
                  STATUS_HUE_Y + STATUS_HUE_HEIGHT + 1, color);
    gui.DrawFrame(STATUS_BRI_X-1,
                  STATUS_BRI_Y-1,
                  STATUS_BRI_X + STATUS_BRI_WIDTH + 1,
                  STATUS_BRI_Y + STATUS_BRI_HEIGHT + 1, color);
}

hsv guiHelper::valueToHsv(const HueValue& v) {
    hsv hsv;
    hsv.h = v.getHue() / (float) MAX_HUE * 360.;
    hsv.s = v.getSaturation() / (float) MAX_SATURATION;
    hsv.v = v.getBrightness() / (float) MAX_BRIGHTNESS;
    return hsv;
}

hsv guiHelper::rgb2hsv(rgb in)
{
    hsv         out;
    double      min, max, delta;

    min = in.r < in.g ? in.r : in.g;
    min = min  < in.b ? min  : in.b;

    max = in.r > in.g ? in.r : in.g;
    max = max  > in.b ? max  : in.b;

    out.v = max;                                // v
    delta = max - min;
    if (delta < 0.00001)
    {
        out.s = 0;
        out.h = 0; // undefined, maybe nan?
        return out;
    }
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        out.s = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0
        // s = 0, h is undefined
        out.s = 0.0;
        out.h = 0;                            // its now undefined
        return out;
    }
    if( in.r >= max )                           // > is bogus, just keeps compilor happy
        out.h = ( in.g - in.b ) / delta;        // between yellow & magenta
    else
    if( in.g >= max )
        out.h = 2.0 + ( in.b - in.r ) / delta;  // between cyan & yellow
    else
        out.h = 4.0 + ( in.r - in.g ) / delta;  // between magenta & cyan

    out.h *= 60.0;                              // degrees

    if( out.h < 0.0 )
        out.h += 360.0;

    return out;
}


rgb guiHelper::hsv2rgb(hsv in)
{
    //printf("hsv2rgb: Converting %f %f %f\n", in.h, in.s, in.v);
    float      hh, p, q, t, ff;
    long        i;
    rgb         out;

    if(in.s <= 0.0) {       // < is bogus, just shuts up warnings
        out.r = in.v;
        out.g = in.v;
        out.b = in.v;
        return out;
    }
    hh = in.h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in.v * (1.0 - in.s);
    q = in.v * (1.0 - (in.s * ff));
    t = in.v * (1.0 - (in.s * (1.0 - ff)));

    switch(i) {
    case 0:
        out.r = in.v;
        out.g = t;
        out.b = p;
        break;
    case 1:
        out.r = q;
        out.g = in.v;
        out.b = p;
        break;
    case 2:
        out.r = p;
        out.g = in.v;
        out.b = t;
        break;

    case 3:
        out.r = p;
        out.g = q;
        out.b = in.v;
        break;
    case 4:
        out.r = t;
        out.g = p;
        out.b = in.v;
        break;
    case 5:
    default:
        out.r = in.v;
        out.g = p;
        out.b = q;
        break;
    }
    return out;
}

