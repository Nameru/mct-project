/*
 * ConnectionView.cpp
 *
 *  Created on: 19.07.2021
 *      Author: student
 */

#include "ConnectionView.h"
#include "../GuiHelper.h"
#include "../LampGui.h"

extern const uint16_t ConnectBackground[16384];
extern const uint16_t Bridge[16184];

ConnectionView::ConnectionView(lcd_interface* lcd):
                       lcd(lcd), gui(*lcd){
    drawBackground(ConnectBackground);
}

void ConnectionView::drawBackground(const uint16_t image[16384]) {
    uGUI::BMP bmp;
    bmp.height = 128;
    bmp.width  = 128;
    bmp.p      = image;
    bmp.bpp    = 16;
    bmp.colors = BMP_RGB565;
    gui.DrawBMP(0, 0, &bmp);
}


void ConnectionView::tickFunctions() {
    if(needPushButton()) {
        gui.FillScreen(0x000000);
        guiHelper::drawFramesBasedOnConstants(gui);
        guiHelper::drawText(gui, "Push the Bridge Button!", SELECTED_LAMP_NAME_X, SELECTED_LAMP_NAME_Y);
        return;
    }
    //printf("SD connected: %d\n", isSDConnected());
    if(isSDConnected()) {
        gui.FillFrame(BOX_X, SD_BOX_Y, BOX_X + BOX_WIDTH, SD_BOX_Y + BOX_HEIGHT, 0x00ff00);
    }
    //printf("Network connected: %d\n", isNetworkConnected());
    if(isNetworkConnected()) {
        gui.FillFrame(BOX_X, NETWORK_BOX_Y, BOX_X + BOX_WIDTH, NETWORK_BOX_Y + BOX_HEIGHT, 0x00ff00);
    }
    if(isBridgeConnected()) {
        gui.FillFrame(BOX_X, BRIDGE_BOX_Y, BOX_X + BOX_WIDTH, BRIDGE_BOX_Y + BOX_HEIGHT, 0x00ff00);
    }

    if (whereBridge()) {
        gui.FillScreen(0x0);
        drawBackground(Bridge);
        while(true);

    }

}

void ConnectionView::tick() {
    tickFunctions();

    gui.FillFrame(100, 100, 120, 120, 0x000000);
    switch(this->idleTick) {
    case 0:
        gui.FillFrame(100, 100, 110, 110, 0xffffff);
        break;
    case 1:
        gui.FillFrame(110, 100, 120, 110, 0xffffff);
        break;
    case 2:
        gui.FillFrame(110, 110, 120, 120, 0xffffff);
        break;
    case 3:
        gui.FillFrame(100, 110, 110, 120, 0xffffff);
        break;
    default:
        idleTick = 0;
    }
   this->idleTick = (++idleTick)%4;
}

void ConnectionView::setNeedPushButton(function<bool()> b) {
    needPushButton = b;
}

void ConnectionView::setWhereBridge(function<bool()> b) {
    whereBridge = b;
}


void ConnectionView::setSDConnected(function<bool()> b) {
    this->isSDConnected = b;
}
void ConnectionView::setNetworkConnectied(function<bool()> b) {
    this->isNetworkConnected = b;
}
void ConnectionView::setBridgeConnected(function<bool()> b) {
    this->isBridgeConnected = b;
}


