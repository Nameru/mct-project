/*
 * ConnectionView.h
 *
 *  Created on: 19.07.2021
 *      Author: student
 */

#ifndef GUI_CONNECTION_CONNECTIONVIEW_H_
#define GUI_CONNECTION_CONNECTIONVIEW_H_

#include "lcd_interface.h"
#include "uGUI.h"
#include <functional>
using std::function;

#define BOX_X 15
#define BOX_WIDTH 10
#define BOX_HEIGHT 10

#define SD_BOX_Y 41
#define NETWORK_BOX_Y 65
#define BRIDGE_BOX_Y 89

/**
 * This View shows the connection-info screen when starting the MCT.
 * In contrast to the main GUI, this is not inheriting the observer pattern.
 *
 * It contains a set of getter-lambdas. With every tick,
 * the value is retrieved and the gui corresponds with updates.
 * We chose this approach to provide fully loosed coupling when running this parallel in a different thread.
 */
class ConnectionView {

private:
    lcd_interface *lcd;
    uGUI gui;

    int idleTick = 0;

    function<bool()> defaultFunction = [](){return false;};

    function<bool()> isSDConnected = defaultFunction;
    function<bool()> isNetworkConnected = defaultFunction;
    function<bool()> isBridgeConnected = defaultFunction;
    function<bool()> needPushButton = defaultFunction;
    function<bool()> whereBridge = defaultFunction;

public:

    ConnectionView(lcd_interface* lcd);

    void drawBackground(const uint16_t image[16384]);

    void tickFunctions();
    void tick();

    void setNeedPushButton(function<bool()>);
    void setWhereBridge(function<bool()>);
    void setSDConnected(function<bool()>);
    void setNetworkConnectied(function<bool()>);
    void setBridgeConnected(function<bool()>);
};



#endif /* GUI_CONNECTION_CONNECTIONVIEW_H_ */
