/*
 * LampGui.cpp
 *
 *  Created on: 22.06.2021
 *      Author: student
 */


#include "LampGui.h"
#include "GuiHelper.h"


LampGui::LampGui(LampManager *manager, lcd_interface *lcd)
: manager(manager), lcd(lcd), gui(*lcd) {

    drawStartUp();
}

void LampGui::drawStartUp() {
    drawBackground();
    guiHelper::drawText(gui, "Retrieving Lamps", SELECTED_LAMP_NAME_X, SELECTED_LAMP_NAME_Y);


}

void LampGui::drawBackground() {
    gui.FillScreen(0x000000);
    guiHelper::drawFramesBasedOnConstants(gui);
}

void LampGui::drawCurrentColor() {
    HueValue value = manager->getSelectedLamp().getValue();
    //printf("Current value: %s\n", value.toString().c_str());
    guiHelper::hsv hsv = guiHelper::valueToHsv(value);
    guiHelper::rgb rgb = guiHelper::hsv2rgb(hsv);
    UG_COLOR color = value.getOnStatus() ? guiHelper::rgbToInt(rgb): 0x000000;
    gui.FillFrame(CURRENT_VALUE_X,
                  CURRENT_VALUE_Y,
                  CURRENT_VALUE_X + CURRENT_VALUE_WIDTH,
                  CURRENT_VALUE_Y + CURRENT_VALUE_HEIGHT, color);
}

void LampGui::drawLampSelection() {
    gui.FillFrame(LAMP_SELECTION_X, LAMP_SELECTION_Y,
                  LAMP_SELECTION_X + LAMP_SELECTION_WIDTH,
                  LAMP_SELECTION_Y + LAMP_SELECTION_HEIGHT, MAIN_GUI_BACKGROUND);
    guiHelper::drawLampSelectionArrowLeft(gui,
                       LAMP_SELECTION_X,
                       LAMP_SELECTION_Y,
                       LAMP_SELECTION_ARROW_WIDTH,
                       LAMP_SELECTION_HEIGHT);

    guiHelper::drawLampSelectionArrowRight(gui,
                       LAMP_SELECTION_X + LAMP_SELECTION_WIDTH - LAMP_SELECTION_ARROW_WIDTH,
                       LAMP_SELECTION_Y,
                       LAMP_SELECTION_ARROW_WIDTH,
                       LAMP_SELECTION_HEIGHT);


    int count = manager->getLampCount();
    int selected = manager->getSelectedLampId();
    guiHelper::drawDynamicCircles(gui,
                       LAMP_SELECTION_INNER_X,
                       LAMP_SELECTION_Y,
                       LAMP_SELECTION_INNER_WIDTH,
                       LAMP_SELECTION_HEIGHT,
                       LAMP_SELECTION_SPACING,
                       count, selected);

    String name = manager->getSelectedLamp().getName();
    gui.FillFrame(SELECTED_LAMP_NAME_X, SELECTED_LAMP_NAME_Y, 127, 30, 0x0);
    guiHelper::drawText(gui,
                            name,
                            SELECTED_LAMP_NAME_X,
                            SELECTED_LAMP_NAME_Y);
}

void LampGui::drawSatBar() {
    HueValue value = manager->getNewValue();
    guiHelper::drawSatColorBar(gui, value,
                               COLOR_SAT_X,
                               COLOR_SAT_Y,
                               COLOR_SAT_WIDTH,
                               COLOR_SAT_HEIGHT);
}

void LampGui::drawSatBarPointer() {
    int colorSat = manager->getColorSat();
    HueValue value = manager->getSelectedLamp().getValue();
    guiHelper::drawSatPointer(gui, value,
                                      colorSat,
                               COLOR_SAT_X,
                               COLOR_SAT_Y,
                               COLOR_SAT_WIDTH,
                               COLOR_SAT_HEIGHT);
}

void LampGui::drawColorSelectionPointer() {

}


void LampGui::drawColorSelection() {
    guiHelper::drawHueColorMatrix(gui,
                                  COLOR_MATRIX_X,
                                  COLOR_MATRIX_Y,
                                  COLOR_MATRIX_WIDTH,
                                  COLOR_MATRIX_HEIGHT);
}

void LampGui::drawNewColor() {
    HueValue value = manager->getNewValue();
    //printf("New Value: %s\n", value.toString().c_str());
    guiHelper::hsv hsv = guiHelper::valueToHsv(value);
    guiHelper::rgb rgb = guiHelper::hsv2rgb(hsv);
    UG_COLOR color = value.getOnStatus()? guiHelper::rgbToInt(rgb): 0x000000;
    gui.FillFrame(NEXT_VALUE_X,
                  NEXT_VALUE_Y,
                  NEXT_VALUE_X + NEXT_VALUE_WIDTH,
                  NEXT_VALUE_Y + NEXT_VALUE_HEIGHT, color);
}

void LampGui::drawStateSelection() {
    ControlState selected = manager->getControlState();
    guiHelper::drawStateArea(gui, selected);
}

void LampGui::drawBoundingBoxes() {
    guiHelper::drawBoundingBoxesBasedOnConstants(gui);
}

void LampGui::notify(ChangeEvent e) {
    switch(e) {
    case LAMP:
        drawLampSelection();
        drawCurrentColor();
        break;
    case COLOR:
        drawColorSelection();
        drawSatBar();
        break;
    case STATE:
        drawStateSelection();
        drawNewColor();
        break;
    case POINTER:
        drawSatBar();
        //drawColorSelectionPointer();
        //drawSatBarPointer();
        drawNewColor();
        break;
    case ALL:
        drawBackground();
        drawCurrentColor();
        notify(LAMP);
        notify(COLOR);
        notify(STATE);
        notify(POINTER);

    // debug
    //drawBoundingBoxes();
    }
}
