/*
 * GuiHelper.h
 *
 * Some utility functions to improve readability in the main LampGui.cpp
 *
 *  Created on: 22.06.2021
 *      Author: student
 */

#ifndef GUI_GUIHELPER_H_
#define GUI_GUIHELPER_H_

#include "uGUI.h"
#include "font_5x8.h"
#include "posix_io.h"
#include "GuiConstants.h"
#include "LampManager.h"
#include "HueValue.h"

/**
 * The guiHelper namespace provides some functions to keep the GuiClass clean.
 */
namespace guiHelper{

typedef struct {
    float r;       // a fraction between 0 and 1
    float g;       // a fraction between 0 and 1
    float b;       // a fraction between 0 and 1
} rgb;

typedef struct {
    float h;       // angle in degrees
    float s;       // a fraction between 0 and 1
    float v;       // a fraction between 0 and 1
} hsv;

UG_COLOR rgbToInt(rgb in);

hsv valueToHsv(const HueValue&);
hsv   rgb2hsv(rgb in);
rgb   hsv2rgb(hsv in);

void drawLampSelectionArrowLeft(uGUI &gui, int x, int y, int width, int height, UG_COLOR c=MAIN_GUI_COLOR);
void drawLampSelectionArrowRight(uGUI &gui, int x, int y, int width, int height, UG_COLOR c=MAIN_GUI_COLOR);
void drawDynamicCircles(uGUI &gui, int x, int y, int width, int height, int minSpacing, int amount, int selectedIndex, UG_COLOR c=MAIN_GUI_COLOR);
void drawText(uGUI &gui, String text, int x, int y, const uGUI::FONT *font=&FONT_5X8, UG_COLOR c=MAIN_GUI_COLOR, UG_COLOR back=0x000000);
void drawHueColorMatrixPart(uGUI &gui, int x, int y, float pX, float pY);
void drawHueColorMatrix(uGUI &gui, int x, int y, int width, int height);
void drawSatColorPart(uGUI &gui, hsv hsv, int x, int y, int width, float pY);
void drawSatColorBar(uGUI &gui, HueValue& v, int x, int y, int width, int height);
void drawSatPointer(uGUI &gui, HueValue& v, int colorSat, int x, int y, int width, int height, int dy=5);
void drawStateArea(uGUI &gui, ControlState selectedState);
void drawBoundingBoxesBasedOnConstants(uGUI &gui);
void drawFramesBasedOnConstants(uGUI &gui, UG_COLOR color = MAIN_GUI_COLOR);

hsv valueToHsv(const HueValue& v);
hsv rgb2hsv(rgb in);
rgb hsv2rgb(hsv in);
}

#endif /* GUI_GUIHELPER_H_ */
