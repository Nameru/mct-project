/*
 * ButtonController.h
 *
 *  Created on: 05.06.2021
 *      Author: student
 */

#ifndef CONTROLS_INTERRUPTBUTTONCONTROLLER_H_
#define CONTROLS_INTERRUPTBUTTONCONTROLLER_H_

#include "gpio_msp432.h"
#include "adc14_msp432.h"
#include "../LampManager.h"
#include "task.h"
#include "posix_io.h"

#define BTN_NEXT_LAMP PORT_PIN(5, 1)
#define BTN_APPLY PORT_PIN(3, 5)
#define BTN_NETWORK_1 PORT_PIN(1, 1)
#define BTN_NETWORK_2 PORT_PIN(1, 4)
#define BTN_TOGGLE_CONTROL_STATE PORT_PIN(4, 1)
#define ADC_STICK_X 15
#define ADC_STICK_Y 9

// controls how fast the stick will call when holding
#define STICK_SLEEP_INTERVAL 100

/**
 * Buttoncontroller that works by registering interrupts for the specific events.
 *
 * This is not usable in productive due to limitations in the yahal library.
 * 1) it is not possible to define a C++ lambda for ADC-Ports.
 * 2) Button Contact bounce (Prellung) cannot be avoided by calling task::sleep inside an interrupt.
 *
 */
class InterruptButtonController {
private:

    LampManager* lampManager;

    // buttons on the right side of the booster pack
    gpio_msp432_pin btnNextLamp;
    gpio_msp432_pin btnApply;

    // buttons on the base plate
    gpio_msp432_pin btnNetwork1;
    gpio_msp432_pin btnNetwork2;

    // stick
    gpio_msp432_pin btnToggleControlState;
    adc14_msp432_channel stickX;
    adc14_msp432_channel stickY;

    uint16_t offsetX, offsetY;

public:
    InterruptButtonController(LampManager* lampManager)
    : lampManager(lampManager),
      btnNextLamp(BTN_NEXT_LAMP),
      btnApply(BTN_APPLY),
      btnNetwork1(BTN_NETWORK_1),
      btnNetwork2(BTN_NETWORK_2),
      btnToggleControlState(BTN_TOGGLE_CONTROL_STATE),
      stickX(ADC_STICK_X),
      stickY(ADC_STICK_Y) {


        btnNextLamp.gpioAttachIrq(GPIO::FALLING, [=]() {
            task::sleep(10);
            printf("button nextLamp\n");
            lampManager->selectNextLamp();
        });
        btnApply.gpioAttachIrq(GPIO::FALLING, [=]() {
            task::sleep(10);
            printf("button apply\n");
            lampManager->applyValue();
        });

        btnNetwork1.gpioMode(GPIO::INPUT | GPIO::PULLUP);
        btnNetwork1.gpioAttachIrq(GPIO::FALLING, [=]() {
            task::sleep(100);
            printf("button network1\n");
            lampManager->establishNetworkConnection();
        });

        function<void()> handler = [&]() {
            handleControl();
        };


        btnToggleControlState.gpioAttachIrq(GPIO::FALLING, handler);


        // initialize pins
        // adc input  0 .. 1023 range
        stickX.adcMode(ADC::ADC_10_BIT);
        stickY.adcMode(ADC::ADC_10_BIT);
        offsetX = stickX.adcReadScan();
        offsetY = stickY.adcReadScan();

        void (*handler2)(uint16_t chan, uint16_t value) = [](uint16_t a,uint16_t b) {

        };
        stickX.attachScanIrq(handler2);

        // TODO ask
        // Wie geht der adc function pointer zu einer member-function?
    }

    void handleControl() {
            task::sleep(50);
            printf("button controlState\n");
            lampManager->toggleControlState();

    }

    void handleStickX(uint16_t chan, uint16_t value) {
        if(value - offsetX > 200) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(1, 0);
        } else if(value - offsetX < -200) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(-1, 0);
        }
    }

    void handleStickY(uint16_t chan, uint16_t value) {
        if(value - offsetY > 200) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(0, 1);
        } else if(value - offsetY < -200) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(0, -1);
        }
    }
};


#endif /* CONTROLS_INTERRUPTBUTTONCONTROLLER_H_ */
