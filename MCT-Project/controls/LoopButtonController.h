/*
 * LoopButtonController.h
 *
 *  Created on: 25.06.2021
 *      Author: student
 */

#ifndef CONTROLS_LOOPBUTTONCONTROLLER_H_
#define CONTROLS_LOOPBUTTONCONTROLLER_H_



#include "gpio_msp432.h"
#include "adc14_msp432.h"
#include "../LampManager.h"
#include "task.h"
#include "posix_io.h"

#define BTN_NEXT_LAMP PORT_PIN(5, 1)
#define BTN_APPLY PORT_PIN(3, 5)
#define BTN_TOGGLE_CONTROL_STATE PORT_PIN(4, 1)
#define ADC_STICK_X 15
#define ADC_STICK_Y 9

// controls how fast the stick will call when holding
#define STICK_SLEEP_INTERVAL 50

/**
 * Buttoncontroller that works by checking buttons in a loop method.
 *
 * Pressing buttons will result in calling LampManager methods.
 *
 * As there is no ActionQueue in the LampManager, the Thread containing the button
 * press needs enough stack capacity to execute its command.
 *
 * Stick movement is split into three zones: Deadzone, Normal-zone and fastzone.
 * The Deadzone marks the area in which stick movement will not be delegated to the manager.
 * Inside the normal zone, the Manager will be informed every tick about the stick pointing in a direction.
 * This will then change i. e. the Saturation of the value.
 * The Fastzone is for better User experience as it informs the manager a second time,
 * resulting in doubled speed when traversing the color matrix.
 */
class LoopButtonController {
private:

    LampManager* lampManager;

    // buttons on the right side of the booster pack
    gpio_msp432_pin btnNextLamp;
    gpio_msp432_pin btnApply;

    // stick
    gpio_msp432_pin btnToggleControlState;
    adc14_msp432_channel stickX;
    adc14_msp432_channel stickY;

    int sleepInterval = 20;

    uint16_t offsetX, offsetY;
    int stepSize = 1;
    int deadzone = 200;

    // zone where moving speed is doubled
    int fastzone = 300;


public:
    LoopButtonController(LampManager* lampManager)
    : lampManager(lampManager),
      btnNextLamp(BTN_NEXT_LAMP),
      btnApply(BTN_APPLY),
      btnToggleControlState(BTN_TOGGLE_CONTROL_STATE),
      stickX(ADC_STICK_X),
      stickY(ADC_STICK_Y) {

        // initialize pins
        // adc input  0 .. 1023 range
        stickX.adcMode(ADC::ADC_10_BIT);
        stickY.adcMode(ADC::ADC_10_BIT);
        offsetX = stickX.adcReadScan();
        offsetY = stickY.adcReadScan();
    }

    void startAllTasks() {
        printf("Starting all ButtonControl tasks...");
        (new task([&](){loopSelectionButton();}, "loopSelection", 2048))->start();
        (new task([&](){loopApplyButton();}, "apply", 2048))->start();
        (new task([&](){loopStateToggleButton();}, "stateToggle", 2048))->start();

        /* split x and y to own threads
        (new task([&](){loopStickX();}, "stickX"))->start();
        (new task([&](){loopStickY();}, "stickY"))->start();
        */

        // combined
        (new task([&](){loopStick();}, "stick", 2048))->start();




        printf("done\n");
    }

    void loopSelectionButton() {
        while(true) {
            bool lampSelection = false;
            while(btnNextLamp.gpioRead() == 0) {
                task::sleep(sleepInterval);
                lampSelection=true;
            }
            if(lampSelection) {
                lampManager->selectNextLamp();
            }
        }
    }

    void loopApplyButton() {
        while(true) {
            bool pressed = false;
            while(btnApply.gpioRead() == 0) {
                task::sleep(sleepInterval);
                pressed=true;
            }
            if(pressed) {
                lampManager->applyValue();
            }
        }
    }

    void loopStateToggleButton() {
        while(true) {
            bool pressed = false;
            while(btnToggleControlState.gpioRead() == 0) {
                task::sleep(sleepInterval);
                pressed=true;
            }
            if(pressed) {
                lampManager->toggleControlState();
            }
        }
    }

    void loopStick() {
        while(true) {
            handleStickX(stickX.adcReadRaw());
            handleStickY(stickY.adcReadRaw());
        }

    }

    void loopStickX() {
        while(true) {
            handleStickX(stickX.adcReadRaw());
        }
    }

    void loopStickY() {
        while(true) {
            handleStickY(stickY.adcReadRaw());
        }
    }

    void handleControl() {
            task::sleep(50);
            printf("button controlState\n");
            lampManager->toggleControlState();

    }

    void handleStickX(uint16_t value) {
        if(value - offsetX > deadzone) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(stepSize, 0);
        } else if(value - offsetX < -deadzone) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(-stepSize, 0);
        }
        handleFastzoneX(value);
    }

    void handleFastzoneX(uint16_t value) {
        if(value - offsetX > fastzone) {
            lampManager->moveValueCursor(stepSize, 0);
        } else if(value - offsetX < -fastzone) {
            lampManager->moveValueCursor(-stepSize, 0);
        }
    }

    void handleStickY(uint16_t value) {
        if(value - offsetY > deadzone) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(0, stepSize);
        } else if(value - offsetY < -deadzone) {
            task::sleep(STICK_SLEEP_INTERVAL);
            lampManager->moveValueCursor(0, -stepSize);
        }
        handleFastzoneY(value);
    }

    void handleFastzoneY(uint16_t value) {
        if(value - offsetY > fastzone) {
            lampManager->moveValueCursor(0, stepSize);
        }else if(value - offsetY < -fastzone) {
            lampManager->moveValueCursor(0, -stepSize);
        }
    }


    void setSleepInterval(int interval) {
        this->sleepInterval = interval;
    }
    int getSleepInterval() {
        return sleepInterval;
    }
};



#endif /* CONTROLS_LOOPBUTTONCONTROLLER_H_ */
