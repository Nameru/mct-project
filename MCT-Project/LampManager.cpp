/*
 * LampManager.cpp
 *
 *  Created on: 28.05.2021
 *      Author: student
 */
#include "LampManager.h"
#include "posix_io.h"

LampManager::LampManager(LampModel* model): model(model), newValue(0,0,0,true)  {
    lamps = new HueLamp[MAX_LAMPS];
    controlState = ControlState::HUE;
    newValue.percentBrightness(colorBri);
    newValue.percentHue(colorHue);
    newValue.percentSaturation(colorSat);
}

LampManager::~LampManager() {
    delete[] lamps;
}

int LampManager::getLampCount() {
    return lampCount;
}

void LampManager::addLamp(HueLamp lamp) {
    lamps[lampCount++] = lamp;
}

void LampManager::printLamps() {
    printf("Lampcount: %d\n", lampCount);
    for(int i = 0; i < lampCount; i++) {
        HueLamp lamp = lamps[i];
        printf("lamp[%d]: %s\n", i, lamp.toString().c_str());
    }
}

void LampManager::setView(Observer *observer) {
    this->view = observer;
}

void LampManager::notifyView(ChangeEvent e) {
    if(view != 0) {
        view->notify(e);
    }
}

void LampManager::reloadLamps() {
    lampCount = model->getLamps(lamps, MAX_LAMPS);
    selectedLamp = 0;
    notifyView(ALL);
}

void LampManager::selectLamp(int idx) {
    this->selectedLamp = idx;
    notifyView(ALL);
}

bool LampManager::containsLampWithHueId(int id) {
    for(int i = 0; i < lampCount; ++i) {
        if(lamps[i].getId() == id) {
            return true;
        }
    }
    return false;
}

HueLamp LampManager::getLampByHueId(int id) {
    for(int i = 0; i < lampCount; ++i) {
        if(lamps[i].getId() == id) {
            return lamps[i];
        }
    }
    return HueLamp();
}

HueLamp LampManager::getSelectedLamp() {
    return lamps[selectedLamp];
}

int LampManager::getSelectedLampId() {
    return selectedLamp;
}

HueValue LampManager::getNewValue() {
    return newValue;
}

ControlState LampManager::getControlState() {
    return controlState;
}

void LampManager::changeValueHue(int dx, int dy) {
    colorHue += dx;
    colorBri += dy;

    // hue value should circle
    colorHue = colorHue < 0 ? 100: colorHue > 100 ? 0: colorHue;
    colorBri = colorBri < 0 ? 0: colorBri > 100 ? 100: colorBri;

    newValue.percentHue(colorHue);
    newValue.percentBrightness(colorBri);
}

void LampManager::changeValueSaturation(int dy) {
    colorSat += dy;
    colorSat = colorSat < 0 ? 0: colorSat > 100 ? 100: colorSat;
    newValue.percentSaturation(colorSat);
}

int LampManager::getColorX() {
    return colorHue;
}

int LampManager::getColorY() {
    return colorBri;
}

int LampManager::getColorSat() {
    return colorSat;
}

/******** button interactions ***********/
void LampManager::selectNextLamp()  {
    selectedLamp++;
    if(selectedLamp >= lampCount) {
        selectedLamp = 0;
    }
    notifyView(LAMP);
}

void LampManager::establishNetworkConnection() {
    model->establishConnection();
}

void LampManager::applyValue() {
    model->updateLampValue(getSelectedLamp().getId(), newValue);
    lamps[getSelectedLampId()].setValue(newValue);
    notifyView(LAMP);
}

void LampManager::moveValueCursor(int dx, int dy) {
    switch(controlState) {
    case OFF: // no effect when turning on
        break;
    case HUE:
        changeValueHue(dx, dy);
        break;
    case BRIGHTNESS:
        changeValueSaturation(dy);
        break;
    }
    notifyView(POINTER);
}

void LampManager::toggleControlState() {
    switch (controlState) {
    case OFF:
        newValue.setOnStatus(true);
        controlState = HUE; break;
    case HUE:
        controlState = BRIGHTNESS; break;
    case BRIGHTNESS:
        newValue.setOnStatus(false);
        controlState = OFF; break;
    }
    notifyView(STATE);
}

