/*
 * LampModel.cpp
 *
 *  Created on: 03.06.2021
 *      Author: student
 */


#include <model/LampModel.h>
#include "yahal_String.h"
#include "Json.h"
#include "posix_io.h"

void LampModel::establishConnection() {
    restFacade->establishConnection();
}

int LampModel::getLamps(HueLamp* lamps, int maxAmount) {

    int i = 1;
    int lampCount = 0;

    while(i++ <= maxAmount) {

        String endpoint = "lights/" + to_String(i);
        String resp = restFacade->get(endpoint);

        // error message starts with [
        if(resp.c_str()[0] == '[') {
            break;
        }

        HueLamp lamp = Json::toHueLamp(resp);

        if(lamp.isReachable()) {
            lamp.setId(i);
            lamps[lampCount] = lamp;
            lampCount++;
        }
    }
    return lampCount;
}

HueLamp LampModel::getLamp(int id) {

    String endpoint = "lights/" + to_String(id);
    String resp = restFacade->get(endpoint);

    HueLamp lamp = Json::toHueLamp(resp);

    lamp.setId(id);

    return lamp;
}

void LampModel::updateLampValue(int lampId, HueValue value) {

    String endpoint = "lights/" + to_String(lampId) + "/state";
    String body = Json::toJson(value);

    String resp = restFacade->put(endpoint, body);
}




