/*
 * RestFacade.h
 *
 *  Created on: 01.06.2021
 *      Author: student
 */

#ifndef MODEL_RESTFACADE_H_
#define MODEL_RESTFACADE_H_

#include "yahal_String.h"
#include "uart_msp432.h"

// help struct for incoming Messages
struct Message {
    String body;
    bool waiting_for_answer;
};

/**
 * The RestFacade encapsulates the REST logic.
 * It provides a simple interface with the basic GET, POST, PUT methods and hides the underlying communication with the ESP8266.
 *
 * It parses these HTTP-calls and sends commands to the ESP while also receiving and processing the ESP's answers.
 *
 * It provides simple status-bools for setting the Connection-GUI.
 */
class RestFacade {
private:

    String ssid;
    String password;

    String bridgeIp;
    String userId;
    uart_msp432 *uart_esp;

    String base_url;

    Message incoming_message;
    String answer;

    // booleans for updating the view
    bool networkConnection = false;
    bool bridgeConnection  = false;
    bool pushButton        = false;
    bool whereBridge       = false;


public:
    RestFacade(uart_msp432 *uart_esp);
    RestFacade();
    ~RestFacade();

    void establishConnection();
    bool checkUserId();
    void createUser();
    String getBaseUrl();

    bool getNetworkConnection();
    bool getBridgeConnection();
    bool getPushButton();
    bool getWhereBridge();

    String get(String endpoint, String body = "");
    String put(String endpoint, String body);
    String post(String endpoint, String body);

    void receive_handler(char c);
    void process_answer();

    // compare two c_str()
    // return true, if both strings has same characters
    bool compare_string(const char * left, const char * right );
};


#endif /* MODEL_RESTFACADE_H_ */
