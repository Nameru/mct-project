/*
 * Json.cpp
 *
 *  Created on: 01.06.2021
 *      Author: student
 */

#include <model/Json.h>
#include "yahal_String.h"
#include <string.h>
#include "posix_io.h"
#include "task.h"

String Json::toJson(bool b) {
    if(b) {
        return "true";
    }
    return "false";
}

String Json::toJson(HueValue value) {
    String json = "{";
    json += "\"sat\": " + to_String(value.getSaturation()) + ", ";
    json += "\"bri\": " + to_String(value.getBrightness()) + ", ";
    json += "\"hue\": " + to_String(value.getHue()) + ", ";
    json += "\"on\": " + Json::toJson(value.getOnStatus());
    json += "}";
    return json;
}


HueValue Json::toHueValue(String s) {
    char* stringBuffer = new char[s.size()+1];
    strcpy(stringBuffer, s.c_str());
    json_t pool[MAX_JSON_FIELDS];
    json_t const *parent = json_create(stringBuffer, pool, MAX_JSON_FIELDS);


    HueValue value = toHueValue(parent);

    delete[] stringBuffer;

    return value;
}

HueValue Json::toHueValue(json_t const * parent) {
    HueValue value;

    json_t const* satField = json_getProperty(parent, "sat");
    json_t const* briField = json_getProperty(parent, "bri");
    json_t const* hueField = json_getProperty(parent, "hue");
    json_t const* onField = json_getProperty(parent, "on");

    value.setSaturation(json_getInteger(satField));
    value.setBrightness(json_getInteger(briField));
    value.setHue(json_getInteger(hueField));
    value.setOnStatus(json_getBoolean(onField));

    return value;
}

HueLamp Json::toHueLamp(json_t const * json) {
    HueLamp lamp;

    //lamp.setId(json_getName(json));

    json_t const* stateField = json_getProperty(json, "state");
    lamp.setValue(toHueValue(stateField));

    json_t const* reachableField = json_getProperty(stateField, "reachable");
    lamp.setReachable(json_getBoolean(reachableField));

    json_t const* nameField = json_getProperty(json, "name");
    lamp.setName(json_getValue(nameField));


    return lamp;
}

HueLamp Json::toHueLamp(String s) {
    char* stringBuffer = new char[s.size()+1];
    strcpy(stringBuffer, s.c_str());
    json_t pool[MAX_JSON_FIELDS];

    json_t const *parent = json_create(stringBuffer, pool, MAX_JSON_FIELDS);

    return toHueLamp(parent);
}

int Json::toHueLampArray(String s, HueLamp* lamps, int size) {
    char* stringBuffer = new char[s.size()+1];
    strcpy(stringBuffer, s.c_str());
    json_t pool[MAX_JSON_FIELDS];

    json_t const *parent = json_create(stringBuffer, pool, MAX_JSON_FIELDS);

    if(parent == 0) {
        printf("Parent is nullPointer\n");
    } else {
        printf("Parent is not null\n");
    }

    int i = 1;
    const char * index;
    json_t const* lamp;

    for(i = 1; i < size; ++i) {
        HueLamp l;
        lamps[i-1] = l;//toHueLamp(lamp);
        lamps[i-1].setId(i);
    }

    //delete parent;
    printf("After delete\n");

    return i-1;
}

DiscoverMessage Json::toDiscoverMessage(String json) {
    DiscoverMessage message;
    message.ip = "";
    char* stringBuffer = new char[json.size()+1];
    strcpy(stringBuffer, json.c_str());
    json_t pool[5];
    json_t const *parent = json_create(stringBuffer, pool,
                                       MAX_JSON_FIELDS);


    // the answer may be wrapped in []
    if(stringBuffer[0] == '[') {
        parent = json_getChild(parent);
    }
    json_t const* ipField = json_getProperty(parent, "internalipaddress");
    message.ip = json_getValue(ipField);

    return message;
}

RegisterAnswer Json::toRegisterAnswer(String json) {
    RegisterAnswer answer;
    char* stringBuffer = new char[json.size()+1];
    strcpy(stringBuffer, json.c_str());
    json_t pool[10];
    json_t const *parent = json_create(stringBuffer, pool,
                                       MAX_JSON_FIELDS);

    // the answer may be wrapped in []
    if(stringBuffer[0] == '[') {
        parent = json_getChild(parent);
    }
    json_t const* successField = json_getProperty(parent, "success");
    if(successField) {
        answer.success = true;

        json_t const* usedIdField = json_getProperty(successField, "username");
        answer.userId = json_getValue(usedIdField);
        return answer;
    }

    answer.success = false;

    json_t const* errorField = json_getProperty(parent, "error");
    json_t const* msgField = json_getProperty(errorField, "description");
    answer.userId = json_getValue(msgField);

    return answer;
}

String Json::toJson(RegisterMessage& message) {
    String json = "{";
    json += "\"devicetype\":\"";
    json += message.devicename;
    json += "\"}";
    return json;
}
