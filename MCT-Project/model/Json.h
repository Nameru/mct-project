/*
 * Json.h
 *
 *  Created on: 01.06.2021
 *      Author: student
 */

#ifndef MODEL_JSON_H_
#define MODEL_JSON_H_

#include "yahal_String.h"
#include "lib/tiny-json.h"
#include "HueLamp.h"
#include "HueValue.h"

#define MAX_JSON_FIELDS 500

// wrapper for the register call
typedef struct RegisterMessage {
    String devicename;
} RegisterMessage;

// wrapper for the register answer
typedef struct RegisterAnswer {
    bool success;
    String userId;
} RegisterAnswer;

// wrapper for the discovery message
typedef struct DiscoverMessage {
    String ip;
} DiscoverMessage;


/**
 * The Json namespace provides some utility-functions for transforming entities into json and back.
 * They are used to fill the body of HTTP requests.
 */
namespace Json {

     String toJson(bool b);

     HueValue toHueValue(json_t const*);
     HueValue toHueValue(String json);
     HueLamp toHueLamp(json_t const*);
     HueLamp toHueLamp(String json);

    /**
     * Parses a given json String into a given array.
     * The size corresponds to the maximum size of the given array
     *
     * The amount of total lamps is returned
     */
     int toHueLampArray(String json, HueLamp*, int size);

     RegisterAnswer toRegisterAnswer(String json);
     DiscoverMessage toDiscoverMessage(String json);

     String toJson(RegisterMessage& message);
     String toJson(HueValue);
};


#endif /* MODEL_JSON_H_ */
