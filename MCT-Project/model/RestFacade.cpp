/*
 * RestFacade.cpp
 *
 *  Created on: 01.06.2021
 *      Author: student
 *
 *      UART wird benutzt um MSP und ESP miteinander
 *      kommunizieren zu lassen
 */

#include "RestFacade.h"
#include <model/Json.h>
#include <properties/Properties.h>
#include "Commands.h"

#include <cstring>
#include <cstdio>

#include "posix_io.h"
#include "uart_msp432.h"
#include "gpio_msp432.h"
#include "msp.h"
#include "task.h"
#include "uart_msp432.h"


RestFacade::RestFacade(uart_msp432 *uart_esp):uart_esp(uart_esp) {


    // get all Information from SD Card
    ssid     = Properties::getInstance()->getSSID();
    password = Properties::getInstance()->getKey();
    userId   = Properties::getInstance()->getApiUserId();

    // Receive message handler:
    // All received chars are collected in a String (answer).

    uart_esp->uartAttachIrq([&](char c) {
            receive_handler(c);
            });

}


RestFacade::RestFacade() {
}

RestFacade::~RestFacade() {
}

void RestFacade::establishConnection(){

    //configure ESP
    //send SSID and PW

    incoming_message.waiting_for_answer = true;
    // waiting for boot time of esp
    task::sleep(2000);
    // sending ssid and password

    uart_esp->puts(Commands::SSID + ssid + Commands::KEY + password);


    while(incoming_message.waiting_for_answer);

    // set IP Address of Hue Bridge
    DiscoverMessage msg = Json::toDiscoverMessage(incoming_message.body);
    bridgeIp = msg.ip;
    networkConnection = true;

    // überprüfe user ID
    printf("Checking existing userID\n");
    if (!checkUserId()){
        createUser();
    }

    base_url = getBaseUrl();
    bridgeConnection = true;

    // save userId on SD Card
    Properties::getInstance()->setApiUserId(userId);
    Properties::getInstance()->save();
    printf("Established Connection! %s\n", base_url.c_str());
}

bool RestFacade::checkUserId(){

    // create new base_url after getting new user id
    base_url = getBaseUrl();
    // send connection to lights/1
    String answer = get("lights/1");

    if (answer[0] == '['){
        printf("first char %c\n", answer[0]);
        return false;
    }

    return true;

}

void RestFacade::createUser(){

    RegisterMessage device;
    device.devicename = "esp";
    pushButton = false;

    String post_message = post("", Json::toJson(device));
    printf("Post response: %s\n", post_message.c_str());
    RegisterAnswer answer = Json::toRegisterAnswer(post_message);

    userId = answer.userId;

    while (compare_string(userId.c_str(), "link button not pressed")){
        pushButton = true;
        // click on hue button
        // wait 10 seconds for the user
        task::sleep(10000);
        post_message = post("", Json::toJson(device));
        answer = Json::toRegisterAnswer(post_message);

        userId = answer.userId;
        printf("2. receive: %s\n", userId.c_str());

    }

    printf("3. receive: %s\n", userId.c_str());
    pushButton = false;
    printf("user created\n");

}

String RestFacade::getBaseUrl() {
    // return base url
    return "http://" + bridgeIp + "/api/" + userId + "/";
}

bool RestFacade::getNetworkConnection(){
    return networkConnection;
}

bool RestFacade::getBridgeConnection(){
    return bridgeConnection;
}

bool RestFacade::getPushButton(){
    return pushButton;
}

bool RestFacade::getWhereBridge(){
    return whereBridge;
}

String RestFacade::get(String endpoint, String body) {
    /*
     * aufruf: get("lights", "");
     *
     * String endpoint = String zum schicken an des usp
     *
     * while (waiting for answer)
     * empfange daten
     *
     * return empfangene Daten
     *
     */
    // set waiting for incoming message to true;
    incoming_message.waiting_for_answer = true;


    // building endpoint
    String data = base_url + endpoint;
    task::enterCritical();
    // send message body
    uart_esp->puts(Commands::GET + data);

    // send END sign
    // uart_esp->puts(Commands::END);
    //task::sleep(1000);
    while(incoming_message.waiting_for_answer);
    task::leaveCritical();

    return incoming_message.body;

}

String RestFacade::put(String endpoint, String body) {

    // set waiting for incoming message to true;
    incoming_message.waiting_for_answer = true;

    // send endpoint
    String data = base_url + endpoint;

    // send body
    uart_esp->puts(Commands::PUT + data + Commands::BODY + body + "\n");

    while(incoming_message.waiting_for_answer);

    return incoming_message.body;

}
String RestFacade::post(String endpoint, String body) {

    // set waiting for incoming message to true;
    incoming_message.waiting_for_answer = true;

    // send endpoint
    String data = "http://" + bridgeIp + "/api/" + endpoint;
    // and body
    uart_esp->puts(Commands::POST + data + Commands::BODY + body);

    while(incoming_message.waiting_for_answer);

    return incoming_message.body;
}

void RestFacade::receive_handler(char c){
    // get from esp
    // if answer make message handler
    if (c == '\n') {
        if (compare_string(
                answer.substr(0, Commands::BODY.size()).c_str(),
                Commands::BODY.c_str())){
            //printf("body substring!\n");
            incoming_message.body =
                    answer.substr(Commands::BODY.size(), -1);
            incoming_message.waiting_for_answer = false;
            if (compare_string(incoming_message.body.c_str(), Commands::ERROR.c_str())){
                // checking if Bridge available
                whereBridge = true;
            }
        } else {
            printf("I received a message from ESP8266: %s\n", answer.c_str());
        }
        answer.clear();
    } else {
        answer += c; // Add char to message
    }
}


bool RestFacade::compare_string(const char * left, const char * right ){

    // compare two equal char ptr
    // return true, if left == right

    int i = 0;
    while (left[i] == right[i]){
        // compare if both const char * ends to \0
        if (left[i] == '\0' && right[i] == '\0'){
            return true;
        }
        i++;
    }
    return false;
}
