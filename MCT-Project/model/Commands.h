/*
 * Commands.h
 *
 *  Created on: Jul 1, 2021
 *      Author: xxx
 */

#ifndef MODEL_COMMANDS_H_
#define MODEL_COMMANDS_H_

#include <cstdint>


/**
 * The Commands are used for communication between the RestFacade and the ESP.
 * They serve as Keywords to split a stream of chars into different meanings i. e. the http url and body.
 *
 * The reason to use static strings is to provide safety when checking for specific types as the ESP-Project links to the same file.
 */
class Commands {

public:
    static String END;

    static String SSID;
    static String KEY;

    static String GET;
    static String PUT;
    static String POST;
    static String OK;

    static String URL;
    static String BODY;
    static String HEADER;


    static String USER;
    static String IP;

    static String ERROR;

};

inline String Commands::END     = "END";
inline String Commands::SSID    = "SSID";
inline String Commands::KEY     = "KEY";
inline String Commands::GET     = "GET";
inline String Commands::PUT     = "PUT";
inline String Commands::POST    = "POST";
inline String Commands::OK      = "OK";
inline String Commands::URL     = "URL";
inline String Commands::BODY    = "BODY";
inline String Commands::HEADER  = "HEADER";
inline String Commands::USER    = "USER";
inline String Commands::IP      = "IP";
inline String Commands::ERROR   = "ERROR";


#endif /* MODEL_COMMANDS_H_ */
