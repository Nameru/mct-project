/*
 * LampModel.h
 *
 *  Created on: 03.06.2021
 *      Author: student
 */

#ifndef MODEL_LAMPMODEL_H_
#define MODEL_LAMPMODEL_H_

#include <model/RestFacade.h>
#include "HueValue.h"
#include "HueLamp.h"

/**
 * The LampModel encapsulates the HTTP-calls by providing a high-level, abstract interface for the LampManager.
 *
 * It has to convert entities to Json-Strings and calling the correct HTTP-Method.
 * For the HTTP-communication it uses the RESTFacade.
 */
class LampModel {
private:
    RestFacade *restFacade;

public:
    LampModel(RestFacade *restFacade):restFacade(restFacade){}

    void establishConnection();

    // GET api/lights
    /**
     * Writes all lamps into the given array, but stops at maxAmount.
     * The real amount of Lamps is returned.
     */
    int getLamps(HueLamp *array, int maxAmount);

    // GET api/lights/{id}
    HueLamp getLamp(int id);

    bool isLampPresent(int id);

    // PUT api/lights/{id}/state
    void updateLampValue(int lampId, HueValue value);
};


#endif /* MODEL_LAMPMODEL_H_ */
