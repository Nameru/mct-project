/*
 * Config.h
 *
 *  Created on: 24.07.2021
 *      Author: student
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/*
 * This File defines some basic Constants used throughout the Program
 */

// use the given wifi information to connect if nothing is readable from the SD-card
#define WIFI_SSID ""
#define WIFI_KEY ""
#define HUE_USER_ID ""

// use this wifi-info and overwrite infos from the SD-Card
#define FORCE_WIFI_INFO false


/* if set to false, the program will always use the data above.
 *
 * This means, if no HUE_USER_ID is given, it will always ask to register a new bridge user.
 *
 *  SD Pins (Port, Pin)
     blue  (2,0)
     green (2,1)
     orange(2,2)
     yellow(2,3)
     brown  GND
     red    3,3V
 */
#define USE_SD true
#define SD_FILENAME "PROPS"


#endif /* CONFIG_H_ */
