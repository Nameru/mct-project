// main method from esp8266

#include <Arduino.h>
#include <SoftwareSerial.h>

// Commands
#include "Commands.h"

// Wifi bibs
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>

#include <WiFiClientSecureBearSSL.h>
// finger print from meethue
const uint8_t fingerprint[20] = {0x5F, 0xA8, 0xCE, 0x17, 0x15, 0x3B, 0x02, 0x76, 0x57, 0x4E, 0x37, 0xC1, 0xEA, 0x7F, 0x23, 0x41, 0x0E, 0xC3, 0xC5, 0x39};

// Serial Connection to msp
SoftwareSerial msp_uart(4, 5);
// for WiFi Connection
String SSID            = "Asgard";
String PASSWORD        = "Klopfer2Blume";
const char* bridgeIP;//        = "192.168.0.61/";

int status = WL_IDLE_STATUS;

WiFiClient client;
HTTPClient http;


// for incoming messages
struct Message {
    String incoming;
    String command_type;
    String url;
    String body;
};


void setWifi();
void printWifiData();
void printCurrentNet();

// http requests
Message receive();
int  give_pos(String message, String regex);

void https_get    (Message m);
void http_request (Message m);

void send(String payload);


void setup() {
    delay(2000);
    // Serial Bridge to ESP
    Serial.begin(115200);
    // init led
//    led.gpioMode(GPIO::OUTPUT);

    Serial.println();
    delay(20);
    Serial.println("start esp8266");
    delay(20);

}

void loop() {

    // if command_connect
    // set wifi
    // and send ip address from bridge
    // receive loop for incoming data over uart serial bridge
    delay(200);
    struct Message current_message;
    delay(200);
    if (Serial.available()){
        current_message = receive();
        Serial.println("debug current_message");
        delay(20);
        Serial.println("command_type: " + current_message.command_type);
        delay(20);
        Serial.println("url:          " + current_message.url);
        delay(20);
        Serial.println("body:         " + current_message.body);
        delay(20);
        Serial.println("ssid:         " + SSID);
        delay(20);
        Serial.println("password:     " + PASSWORD);
        delay(20);


        if (current_message.command_type == Commands::SSID){
            setWifi();
        }else{
            http_request(current_message);
        }
    }
}

void setWifi(){
    // init wifi
    Serial.println();
    status = WiFi.begin(SSID, PASSWORD);

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED)
    {
//        Serial.print(".");
        // led.gpioToggle();
        delay(500);
    }

    // debug
    // you're connected now, so print out the data:
    Serial.println("You're connected to the network");
    delay(20);
//    printCurrentNet();
//    printWifiData();

    // get hue bridge ip
    // and send it back to esp
    Message bridge_ip;
    bridge_ip.command_type = Commands::GET;
    bridge_ip.url = "https://discovery.meethue.com/";
    https_get(bridge_ip);
}
void printWifiData() {
    //WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);
}

void printCurrentNet() {
    //SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());
}


Message receive(){
    // create Message Object
    Message current_message;
    char c ;
    while((Serial.available())){
        c = Serial.read();
        current_message.incoming += c;
    }

    //building incoming message
    if (current_message.incoming.startsWith(Commands::GET)){
        current_message.command_type = Commands::GET;
        current_message.incoming.replace(Commands::GET, "");
        current_message.url       = current_message.incoming;
    }else if (current_message.incoming.startsWith(Commands::PUT)){
        current_message.command_type = Commands::PUT;
        current_message.incoming.replace(Commands::PUT, "");
        //separate string at "BODY"
        // give position from body
        int pos = give_pos(current_message.incoming, Commands::BODY);

        current_message.url  = current_message.incoming.substring(0, pos);
        current_message.body = current_message.incoming.substring(pos+Commands::BODY.length(), -1);
    }else if (current_message.incoming.startsWith(Commands::POST)){
        current_message.command_type = Commands::POST;
        current_message.incoming.replace(Commands::POST, "");

        // separate string at "BODY"
        // give position from body
        int pos = give_pos(current_message.incoming, Commands::BODY);

        current_message.url  = current_message.incoming.substring(0, pos);
        current_message.body = current_message.incoming.substring(pos+Commands::BODY.length(), -1);
    }else if (current_message.incoming.startsWith(Commands::SSID)){
        //filter ssid and password
        current_message.command_type = Commands::SSID;
        current_message.incoming.replace(Commands::SSID, "");
        int pos = give_pos(current_message.incoming, Commands::KEY);

        SSID     = current_message.incoming.substring(0, pos);
        PASSWORD = current_message.incoming.substring(pos+Commands::KEY.length(), -1);
    }
    return current_message;
}


int give_pos(String incoming, String regex){
    // return position of first char from regex,
    // if regex in incoming String
    // else return -1

    if (incoming.length() < regex.length()){
        return -1;
    }
    bool found;
    for (int i=0 ; i<incoming.length()-regex.length()+1 ; i++) {
        // i: Startposition Muster
        found = true;
        for (int k=0 ; k<regex.length() ; k++) {
            // k: Position im Muster
            if (incoming[i+k] != regex[k]) {
                found = false;
                break;
            }
        }
        if (found) return i;
    }
    return -1;

}


void https_get(Message m){
    /**
    * Method to make a https get request
    * if true, send message body back to msp
    */
    if (WiFi.status() == WL_CONNECTED) {

        std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
        client->setFingerprint(fingerprint);

        if (http.begin(*client, m.url)){
            // start connection and send HTTP header
            int httpCode = http.GET();

            // httpCode will be negative on error
            if (httpCode > 0) {
              // HTTP header has been send and Server response header has been handled
              // file found at server
              if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
                String payload = http.getString();
                Serial.println(Commands::BODY + payload);
              }
            } else {
              Serial.printf("[HTTPS] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
            }

            http.end();
        }else{
          Serial.println("Unable to Connect!");
        }
    }
}

void http_request(Message m){
    /**
    * Main Methods for the esp
    * make Get, Put or Post request to the RESTapi of the hue bridge
    * 
    * if http request True, send body back to msp
    * else send Error message to msp, to avoid crashing of msp
    */
    String payload;
    if (WiFi.status() == WL_CONNECTED) {
        http.begin(client, m.url);
        http.addHeader("Content-Type", "application/json");
        int httpCode = -1;
        if (m.command_type == Commands::GET){
            httpCode = http.GET();
        }
        else if (m.command_type == Commands::PUT){
            httpCode = http.PUT(m.body);
        }
        else if (m.command_type == Commands::POST){
            httpCode = http.POST(m.body);
        }
        if (httpCode > 0){
            payload = Commands::BODY + http.getString();
            send(payload);
            delay(10);

        } else{
            Serial.printf("Error Code: %d\n", httpCode);
            delay(20);
            Serial.printf("m command:  %s\n", m.command_type.c_str());
            delay(20);
            Serial.printf("m url    :  %s\n", m.url.c_str());
            delay(20);
            payload = Commands::BODY + Commands::ERROR;
            send(payload);
            delay(20);
        }
        http.end();
    }
}

void send(String payload){
    /*
    * Method, to send each char over uart to msp
    * Big Strings crashes the msp and are not complete
    */
    const char *chars = payload.c_str();
    delay(1);
    for(int i = 0; i < payload.length(); ++i) {
        Serial.print(chars[i]);
        delay(1);
    }
    delay(10);
    Serial.print('\n');

}
