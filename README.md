# Inhalt
- [Einleitung](README.md#einleitung)
- [Nötige Materialien](README.md#nötige-materialien)
- [Vorbereitung](README.md#vorbereitung)
- [Bedienung](README.md#bedienung)
- [Klassenübersicht](README.md#klassenübersicht)
- [Troubleshooting](README.md#troubleshooting)

# Einleitung 
Dieses Projekt ist eine Fernbedienung für ein Philips-Hue System.


Mit Philips Hue lassen sich glühbirnen im ganzen Haus über eine zentrale Schnittstelle steuern.
Durch eine verbaute RGB-LED, lässt sich zusätzlich zur Helligkeit auch die Farbe einer Birne einstellen.
Die Bridge stellt dafür eine RESTful-API zur Verfügung, mit welcher der Status einer Lampe ausgelesen und aktualisiert werden kann.

### Support
Diese Projekt ist im Rahmen des Semesterkurses Mikrocontrollertechnik an der FH-Aachen im SoSe 21 entstanden.  
Es befindet sich in einem nutzbaren Zustand und wird nicht aktiv weiterentwickelt. Hinzu kommt, dass es sich um ein "Spaßprojekt" handelt, zum erlernen des Entwicklungs- und Programmierungsprozesses von Mikrocontrollern.

# Nötige Materialien
- Ein MSP432P
- Boosterpack
- Wifi-tick ESP8266
- SD-Reader
- Philips Hue Bridge
- Mit Philips Hue kompatible Lampen

# Vorbereitung

Das Hue System muss richtig eingerichtet sein.
Also die Bridge mit dem Netzwerk einrichten und die Glühbirnen registrieren.  
Boosterpack, WiFi-Tick und die SD-Karte an das MSP anschließen.  
Für die SD-Karte werden folgende Pins verwendet (Port, Pin):

- blue:     (2,0)
- green:    (2,1)
- orange:   (2,2)
- yellow:  (2,3)
- brown:   GND
- red:     3,3V

Wifi-Tick flashen, anschließend den MSP flashen.
Aus der SD-Karte werden die WLAN-Informationen sowie die userID für die Bridge ausgelesen und gespeichert.  
Diese können entweder direkt auf die SD-Karte geschrieben (Dateiname: `PROPS`), oder in der Config.h SSID und Passwort angegeben werden. 
Diese werden dann verwendet, falls die Datei noch nicht auf der SD-Karte geschrieben wurde.

# Bedienung

Sobald das Programm gestartet wird, ist auf einem Ladebildschirm der Verbindungsstatus zu sehen.
Ist eine Verbindung erfolgreich, wird das entsprechende Kästchen grün gefärbt.
Bei der ersten Verwendung in einem Hue-Netzwerk, ist eine registrierung in der Bridge notwendig.
Dafür muss, bei Aufforderung des Info-Bildschirms, der Link-Button auf der Bridge gedrückt werden.

Ist die Verbindung erfolgreich, wechselt das Programm zum Hauptbildschirm.
Dort werden zunächst alle erreichbaren Lampen aus dem Netzwerk ausgelesen.
Die maximale Anzahl der Lampen ist mit einem Macro (MAX_HUE_LAMPS) Hardcoded auf 8.
Mit dem oberen Button, kann eine gewünschte Lampe ausgewählt werden.
Der ausgefüllte kreis und der darunter liegende Text zeigen an, welche Lampe aktuell eingestellt wird.

Mit dem Stick lässt sich nun die Farbe einstellen.
Am oberen Rand der Farbmatrix sieht man die aktuelle Farbe der ausgewählten Birne und die neu eingestellte.
Ein Farbton setzt sich aus dem Hue, der Helligkeit und der Sättigung zusammen.
In der Zentralen Matrix sind Hue und Helligkeit gegeben und lassen sich mit dem Stick in X- und Y-Richtung ändern.
Am unteren Rand ist der Modus zu erkennen.
Beim Druck auf den Stick wechselt er zwischen Hue, Sättigung und Aus-Zustand.
Im Sättigungsmodus wird mit der Y-Achse die Sättigung verändert.
Der Aus-Zustand toggelt den aktuellen Zustand einer Glühbirne.
Ist diese aktuell aus, wird also die Lampe eingeschaltet.

Beim drücken des unteren Buttons, wird die Information über die neue Farbe an die ausgewählte glühbirne gesendet.

# Klassenübersicht

![](orga/UML-v2.png)
Dieses UML-Klassendiagramm beschreibt das grobe Verhältnis zwischen den verschiedenen Klassen.
Zur vereinfachung wird auf den ConnectionScreen verzichtet.


Der LampManager übernimmt die Hauptaufgabe: das Speichern und ändern des Farbwertes und der Lampen.
Er wird vom LoopButtonController aufgerufen, wenn der Benutzer Eingaben tätigt.
Er besitzt eine Referenz zum LampModel, der Abstrakten Schnittstelle zur HueBridge.
Die grundlegende Netzwerkverbindung wird nochmals hinter einer RESTFacade verborgen.

Die GUI arbeitet mithilfe des Observer-Patterns nach vereinfachtem Model-View-Viewmodel (MVVM).
Sie ist passiv und wird bei Änderungen im Manager von diesem benachrichtigt.
Mithilfe des ChangeEvents kann ihr genauer mitgeteilt werden, welche Unterkategorie sich verändert hat.
So wird Rechen- und Renderleistung gespart, damit nicht der gesamte Bildschirm neu gemalt werden muss.

Die Main (nicht im Diagramm dargestellt) instanziiert alle nötigen Objekte, bildet die Abhängigkeitskette (View -> Viewmodel -> Model -> RESTFacade) und gibt abschließend die Kontrolle an den LoopButtonController ab.
Während dieses Prozesses ist für den Benutzer der ConnectionScreen zu sehen.
Sobald der LoopButtonController die Kontrolle übernommen hat, reagiert der Mikrocontroller auf die Nutzereingaben.
Jeder Button läuft in einem separatem Thread.
Hier liegt noch verbesserungsbedarf, da viele Parameter mit Call-By-Value übergeben werden.
Das liegt daran, dass bei Planung der Klassen das Multithreading und die damit einhergehende Stackgröße nicht berücksichtigt wurden.
Bei Änderung auf Call-By-Reference könnte hier der Stackbedarf einzelner Threads reduziert werden.

# Troubleshooting

### SD-Karte wird nicht Grün
Sicherstellen, dass die SD-Karte richtig angeschlossen ist.

### Netzwerk wird nicht Grün
Sicherstellen, dass die richtigen Netzwerkdaten auf der SD-Karte eingetragen sind

### Bridgeverbindung wird nicht Grün
Sicherstellen, dass die Hue-Bridge sich im Netzwerk befindet.
Wenn nötig den Link-Button drücken (Bei Aufforderung der GUI)

### Der Bildschirm verhält sich komisch, zeigt die Farben im Negativ an, oder ist komplett weiß
Dieser Fehler taucht manchmal auf, wenn der Stick bewegt wird, während der Button zur Lampenwahl gedrückt wird.
Die bisher einzige Methode das zu umgehen ist ein Reset.
